using Exceptionless;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExceptionlessDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddExceptionless(config =>
            {
                // 自己搭建环境一定要改对应配置
                // config.ApiKey = "MVuYD4jxJyqGM4v8OzMaLJssPVgK8KeSIpDXrGrD";
                // 新建项目时分配的ApiKey
                config.ApiKey = "utFyDen4ZYrZtestT3u7laO009cnjySyh";
            
                // 这个就是我们自己的搭建的Exceptionless Api 环境；  
                // 这里把API和UI部署为一个站点：http://47.test.test.41:5000 ，实际应用场景根据需要任意部署
                config.ServerUrl = "http://47.test.test.41:5000";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseRouting();
            app.UseExceptionless();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
