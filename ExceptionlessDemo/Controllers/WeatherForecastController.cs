﻿using Exceptionless;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExceptionlessDemo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            ExceptionlessClient.Default.SubmitLog("zyq log test debug~~~", Exceptionless.Logging.LogLevel.Debug);
            ExceptionlessClient.Default.SubmitLog("zyq log test error~~~", Exceptionless.Logging.LogLevel.Error);
            ExceptionlessClient.Default.SubmitLog("zyq log test warn~~~", Exceptionless.Logging.LogLevel.Warn);
            ExceptionlessClient.Default.SubmitLog("zyq log test fatal ~~~", Exceptionless.Logging.LogLevel.Fatal);
            ExceptionlessClient.Default.SubmitLog("zyq log test Info ~~~", Exceptionless.Logging.LogLevel.Info);

            ExceptionlessClient.Default.SubmitException(new Exception("异常了，必须处理"));

            try
            {
                throw new Exception("MyApp ToExceptionless error");
            }
            catch (Exception ex)
            {
                // use ToExceptionless extension method. Uses ExceptionlessClient.Default and requires it to be configured.
                ex.ToExceptionless().Submit();
                // don't forget to call Submit.
            }

            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
