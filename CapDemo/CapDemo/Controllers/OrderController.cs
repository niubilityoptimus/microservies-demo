﻿using DotNetCore.CAP;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OrderRespository;
using OrderService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapOderDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly OrderDbContext dbContext;

        public OrderController(OrderDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpPost("GenerateOrder")]
        public async Task<IActionResult> GenerateOrder([FromServices] ICapOrderService capOrderService,
            [FromServices] ICapPublisher capPublisher)
        {
            capOrderService.GenerateOrder();
            return Ok("订单生成成功");
        }
    }
}
