﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SkyWalkingDataDemo.Model
{
    public class User
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string UserPwd { get; set; }
        public int Age { get; set; }
    }
}
